# krakowskiTarg
KrakowskiTarg to aplikacja umożliwiająca dzieleni się usługami na terenie Krakowa. Użytkownik może przeglądać oferty i kontaktować się z osobą, która ofertę wystawiła, by otrzymać wykonanie danej usługi. Każdy użytkownik może się zarejestrować i dodawać własne oferty.

# Drzewo katalogów

- app
    -src
        - main

            - java (główne klasy aplikacji)
                - com.example.bartosz.krakowskitarg
					-
					-
					-
					-
					-
					-
					-
					-

            -res(Zasoby grafika widoki, animacje w jezyku xml)

# Zaimplementowany wzorzec
Zaimplementowany został wzorzec Model View Presenter w całym projekcie.
Presenter zawiera logike biznesową.
Presenter wysyła zapytanie do modelu, model zwraca dane do prezentera, prezenter przetwarza otrzymane dane i przekazuje do widoku.