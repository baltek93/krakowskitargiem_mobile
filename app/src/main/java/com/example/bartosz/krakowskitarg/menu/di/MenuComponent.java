package com.example.bartosz.krakowskitarg.menu.di;

import com.example.bartosz.krakowskitarg.HomeActivityScope;
import com.example.bartosz.krakowskitarg.MyApplicationComponent;
import com.example.bartosz.krakowskitarg.menu.menulist.view.MenuFragment;
import com.example.bartosz.krakowskitarg.offers.di.OfferModule;

import dagger.Component;

/**
 * Created by Bartosz on 22.05.2017.
 */
@HomeActivityScope
@Component(modules = MenuModule.class, dependencies = MyApplicationComponent.class)
public interface MenuComponent {
    void inject(MenuFragment fragment);
}
