package com.example.bartosz.krakowskitarg.menu.di;

import android.content.SharedPreferences;

import com.example.bartosz.krakowskitarg.HomeActivityScope;
import com.example.bartosz.krakowskitarg.data.Repository;
import com.example.bartosz.krakowskitarg.menu.menulist.presenter.MenuPresenter;
import com.example.bartosz.krakowskitarg.menu.menulist.usecase.MenuUseCase;
import com.example.bartosz.krakowskitarg.offers.main.presenter.OfferPresenter;
import com.example.bartosz.krakowskitarg.offers.main.usecase.OfferUseCase;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by Bartosz on 22.05.2017.
 */
@Module
public class MenuModule {
    @Provides
    @HomeActivityScope
    public MenuUseCase providesMenuUseCase(Repository repository, Retrofit retrofit, SharedPreferences sharedPreferences) {
        return new MenuUseCase(repository,retrofit,sharedPreferences);
    }
    @Provides
    @HomeActivityScope
    public MenuPresenter providesMenuPresenter(MenuUseCase usecase) {
        return new MenuPresenter(usecase);
    }
}
