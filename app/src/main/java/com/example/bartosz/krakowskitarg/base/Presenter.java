package com.example.bartosz.krakowskitarg.base;

import org.reactivestreams.Subscription;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * Created by Bartosz on 01.04.2017.
 */

public class Presenter<V extends BaseView> {
    private final UseCase mUseCase;
    protected List<UseCase> mUseCaseList = new ArrayList<>();
    protected CompositeDisposable mSubscriptions = new CompositeDisposable();

    private V mView;

    public Presenter(UseCase mUseCase) {
        addAndReturnUseCase(mUseCase);
        this.mUseCase = mUseCase;
    }
//    public void logout(String errorMessage) {
//
//        query(mUseCase.logoutUser().subscribe(aBoolean ->
//        {
//
//        }));
//
//    }
    public void onViewDestroyed() {
        mSubscriptions.clear();
        mView = null;
    }
    protected void query(Disposable disposable) {
        mSubscriptions.add(disposable);
    }
    protected V getView() {
        return mView;
    }

    public <T extends UseCase> T addAndReturnUseCase(T useCase) {
        mUseCaseList.add(useCase);
        return useCase;
    }

    public final void onViewCreated(V view) {
        mView = view;
        onViewAttached();
    }

    protected void onViewAttached() {
    }

    public void onViewVisible() {

    }

    public void onViewHidden() {

    }
}
