package com.example.bartosz.krakowskitarg.base.util;

import android.app.Dialog;
import android.content.Context;
import android.widget.Toast;

/**
 * Created by Bartosz on 01.04.2017.
 */

public class Util {

    public static void showDialog(Context context, String type) {
        Dialog dialog = new Dialog(context);
        dialog.setTitle(type);
        dialog.show();
    }

    public void showToast(Context context, String name) {
        Toast.makeText(context, name, Toast.LENGTH_LONG).show();
    }

}
