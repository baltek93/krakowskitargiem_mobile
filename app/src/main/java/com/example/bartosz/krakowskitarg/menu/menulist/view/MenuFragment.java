package com.example.bartosz.krakowskitarg.menu.menulist.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.bartosz.krakowskitarg.MainActivity;
import com.example.bartosz.krakowskitarg.MyApplication;
import com.example.bartosz.krakowskitarg.R;
import com.example.bartosz.krakowskitarg.base.BaseFragment;
import com.example.bartosz.krakowskitarg.menu.di.DaggerMenuComponent;
import com.example.bartosz.krakowskitarg.menu.di.MenuModule;
import com.example.bartosz.krakowskitarg.menu.menulist.presenter.MenuPresenter;
import com.example.bartosz.krakowskitarg.offers.main.view.OfferAdapter;

import java.util.ArrayList;

import javax.inject.Inject;

import lombok.Getter;

/**
 * Created by Bartosz on 22.05.2017.
 */

public class MenuFragment extends BaseFragment<MenuPresenter, MenuView> implements MenuView {
    @Inject
    MenuPresenter presenter;
    RecyclerView mRecyclerView;
    MenuAdapter mAdapter;
    @Override
    public int getLayoutId() {
        return R.layout.fragment_menu;
    }

    @Override
    protected void initInjection() {
        DaggerMenuComponent.builder().myApplicationComponent(MyApplication.get(getActivity()).component()).menuModule(new MenuModule()).build().inject(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ArrayList<MenuItem> menuItemArrayList = new ArrayList<>();
        menuItemArrayList.add(new MenuItem("Preferences",R.drawable.ic_log_out));
        menuItemArrayList.add(new MenuItem("Logout",R.drawable.ic_log_out));
        mAdapter= new MenuAdapter(menuItemArrayList,(MainActivity)getActivity());
        mRecyclerView = (RecyclerView) getView().findViewById(R.id.menu_list);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    protected MenuPresenter getPresenter() {
        return presenter;
    }
    @Getter
    public class MenuItem {
        private String labelMenuResources;
        private int idmenuResources;

        public MenuItem(String labelMenuResources, int idmenuResources) {
            this.labelMenuResources = labelMenuResources;
            this.idmenuResources = idmenuResources;
        }
    }
}
