package com.example.bartosz.krakowskitarg.base;

import javax.inject.Qualifier;

/**
 * Created by Bartosz on 26.03.2017.
 */


@Qualifier
public @interface ApplicationContext {
}
