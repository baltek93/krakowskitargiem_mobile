package com.example.bartosz.krakowskitarg.data;


import com.example.bartosz.krakowskitarg.base.LocalRepositoryInterface;

/**
 * Created by Bartosz on 01.04.2017.
 */

public interface Repository extends RemoteRepository, LocalRepositoryInterface {
}
