package com.example.bartosz.krakowskitarg.base;

/**
 * Created by Bartosz on 26.03.2017.
 */

import android.content.Context;
import android.content.SharedPreferences;

import com.example.bartosz.krakowskitarg.MyApplication;
import com.example.bartosz.krakowskitarg.MyApplicationScope;
import com.example.bartosz.krakowskitarg.data.RemoteRepositoryInterface;
import com.example.bartosz.krakowskitarg.data.Repository;
import com.example.bartosz.krakowskitarg.data.RepositoryServices;
import com.fatboyindustrial.gsonjodatime.DateTimeConverter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.joda.time.DateTime;

import dagger.Module;
import dagger.Provides;
import io.realm.Realm;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module(includes = NetworkModule.class)
public class AppModule {
    private  MyApplication myApplication;
    private String PREF_NAME = "prefs";

    public AppModule(MyApplication myApplication) {
        this.myApplication = myApplication;
    }
    @MyApplicationScope
    @Provides
    public SharedPreferences getAppPreferences() {
        return myApplication.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }
    @Provides
    @MyApplicationScope
    public RemoteRepositoryInterface remoteRepository(Retrofit gitHubRetrofit) {
        return gitHubRetrofit.create(RemoteRepositoryInterface.class);
    }

    @Provides
    @MyApplicationScope
    public Repository repository(RemoteRepositoryInterface remoteRepositoryInterface, LocalRepositoryInterface localRepositoryInterface) {
        return new RepositoryServices(remoteRepositoryInterface, localRepositoryInterface);
    }

    @Provides
    public LocalRepositoryInterface localRepository() {
        return new LocalRepository();
    }

    @Provides
    @MyApplicationScope
    public Gson gson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(DateTime.class, new DateTimeConverter());
        return gsonBuilder.create();
    }

    @Provides
    @MyApplicationScope
    public Retrofit retrofit(OkHttpClient okHttpClient, Gson gson) {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .baseUrl("https://krakowski-targ.herokuapp.com/")
                .build();
    }

    @Provides
    Realm provideRealm() {
        return Realm.getDefaultInstance();
    }
}
