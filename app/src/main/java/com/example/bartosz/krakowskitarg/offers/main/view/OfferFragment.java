package com.example.bartosz.krakowskitarg.offers.main.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.bartosz.krakowskitarg.MainActivity;
import com.example.bartosz.krakowskitarg.MyApplication;
import com.example.bartosz.krakowskitarg.R;
import com.example.bartosz.krakowskitarg.data.model.Offer;
import com.example.bartosz.krakowskitarg.offers.di.DaggerOfferComponent;
import com.example.bartosz.krakowskitarg.offers.di.OfferModule;
import com.example.bartosz.krakowskitarg.offers.main.presenter.OfferPresenter;
import com.example.bartosz.krakowskitarg.base.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by Bartosz on 24.04.2017.
 */

public class OfferFragment extends BaseFragment<OfferPresenter, OfferView> implements OfferView {
    @Inject
    OfferPresenter presenter;
    RecyclerView mRecyclerViewOffer;
    OfferAdapter mAdapter;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_offer;
    }

    @Override
    protected void initInjection() {
        DaggerOfferComponent.builder()
                .offerModule(new OfferModule())
                .myApplicationComponent(MyApplication.get(getActivity()).component())
                .build().inject(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mAdapter = new OfferAdapter(new ArrayList<>(), (MainActivity) getActivity());
        mRecyclerViewOffer = (RecyclerView) getView().findViewById(R.id.list_offer);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerViewOffer.setLayoutManager(mLayoutManager);
        mRecyclerViewOffer.setAdapter(mAdapter);

    }

    @Override
    protected OfferPresenter getPresenter() {
        return presenter;
    }

    @Override
    public void renderOffer(List<Offer> offerList) {
        mAdapter.setOfferList(offerList);
    }
}
