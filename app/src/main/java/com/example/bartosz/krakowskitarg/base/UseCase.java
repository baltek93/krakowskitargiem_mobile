package com.example.bartosz.krakowskitarg.base;

import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import com.example.bartosz.krakowskitarg.data.RemoteRepositoryInterface;
import com.example.bartosz.krakowskitarg.data.Repository;
import com.fernandocejas.frodo.annotation.RxLogObservable;

import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

/**
 * Created by Bartosz on 01.04.2017.
 */

public class UseCase {
    protected Repository mRepository;
    protected Retrofit mRetrofit;
    protected RemoteRepositoryInterface mRemoteRepositoryInterface;
    protected SharedPreferences mSharedPreferences;
    protected SharedPreferences.Editor editor;

    public Retrofit getmRetrofit() {
        return mRetrofit;
    }

    public SharedPreferences getSharedPreferences() {
        return mSharedPreferences;
    }

    public SharedPreferences.Editor getEditor() {
        return editor;
    }

    public UseCase(Repository mRepository, Retrofit retrofit, SharedPreferences sharedPreferences) {
        this.mRepository = mRepository;
        this.mRetrofit = retrofit;
        this.mSharedPreferences = sharedPreferences;
        this.editor = mSharedPreferences.edit();
        this.mRemoteRepositoryInterface = mRetrofit.create(RemoteRepositoryInterface.class);
    }

    protected Repository getRepository() {
        return mRepository;
    }

    public final <T> Observable<T> queryLocal(Callable<Observable<T>> func) {
        return getObservable(func).observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(AndroidSchedulers.mainThread());
    }

    public final <T> Observable<T> queryRemote(Callable<Observable<T>> func) {
        return Observable.defer(() -> getObservable(func))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(AndroidSchedulers.mainThread());
    }

    @NonNull
    @RxLogObservable
    public <T> Observable<T> getObservable(Callable<Observable<T>> func) {
        try {
            return func.call().onErrorResumeNext(e -> {
                return Observable.just(((T) getRepository()));
            });
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
