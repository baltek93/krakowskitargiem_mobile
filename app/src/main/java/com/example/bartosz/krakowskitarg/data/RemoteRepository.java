package com.example.bartosz.krakowskitarg.data;

import com.example.bartosz.krakowskitarg.base.AuthorizationToken;
import com.example.bartosz.krakowskitarg.data.model.LoginRequest;
import com.example.bartosz.krakowskitarg.data.model.Offer;


import java.util.List;

import retrofit2.Call;

/**
 * Created by Bartosz on 09.04.2017.
 */

interface RemoteRepository {

    Call<AuthorizationToken> authorization(LoginRequest loginRequest);
    Call<List<Offer>> getOffer(String authorization);

}
