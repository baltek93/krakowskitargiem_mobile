package com.example.bartosz.krakowskitarg.authorization;

import android.os.Bundle;
import android.support.percent.PercentLayoutHelper;
import android.support.percent.PercentRelativeLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.bartosz.krakowskitarg.DaggerHomeActivityComponent;
import com.example.bartosz.krakowskitarg.HomeActivityComponent;
import com.example.bartosz.krakowskitarg.HomeActivityModule;
import com.example.bartosz.krakowskitarg.MyApplication;
import com.example.bartosz.krakowskitarg.R;
import com.example.bartosz.krakowskitarg.authorization.login.di.AuthorizationModule;
import com.example.bartosz.krakowskitarg.authorization.login.di.DaggerAuthorizationComponent;
import com.example.bartosz.krakowskitarg.authorization.login.view.AuthorizationFragment;
import com.example.bartosz.krakowskitarg.base.BaseActivity;
import com.example.bartosz.krakowskitarg.base.Presenter;

import javax.inject.Inject;

public class AuthorizationActivity extends BaseActivity {



    @Override
    protected Presenter getPresenter() {
        return new Presenter(null);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment fragment = new AuthorizationFragment();
        fragmentTransaction.replace(android.R.id.content, fragment);
        fragmentTransaction.commit();

    }

    @Override
    protected void initInjection() {
        DaggerAuthorizationComponent.builder()
                .authorizationModule(new AuthorizationModule())
                .myApplicationComponent(MyApplication.get(getActivity()).component())
                .build().inject(this);

    }

//    @Override
//    protected void initInjection() {
//        HomeActivityComponent component = DaggerHomeActivityComponent.builder()
//                .homeActivityModule(new HomeActivityModule(this))
//                .myApplicationComponent(MyApplication.get(this).component())
//                .build();
//        component.injectHomeActivity(this);
//    }


}
