package com.example.bartosz.krakowskitarg;

import com.example.bartosz.krakowskitarg.authorization.AuthorizationActivity;

import dagger.Component;

@HomeActivityScope
@Component(modules = HomeActivityModule.class, dependencies = MyApplicationComponent.class)
public interface HomeActivityComponent {

    void injectHomeActivity(AuthorizationActivity homeActivity);
}
