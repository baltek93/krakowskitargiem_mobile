package com.example.bartosz.krakowskitarg.offers.detalis.usecase;

import android.content.SharedPreferences;

import com.example.bartosz.krakowskitarg.base.UseCase;
import com.example.bartosz.krakowskitarg.data.Repository;

import retrofit2.Retrofit;



public class OfferDetailsUseCase extends UseCase {
    public OfferDetailsUseCase(Repository mRepository, Retrofit retrofit,
                               SharedPreferences sharedPreferences) {
        super(mRepository, retrofit, sharedPreferences);
    }
}
