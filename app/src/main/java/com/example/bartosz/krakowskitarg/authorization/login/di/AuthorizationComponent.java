package com.example.bartosz.krakowskitarg.authorization.login.di;

import android.support.annotation.NonNull;

import com.example.bartosz.krakowskitarg.HomeActivityModule;
import com.example.bartosz.krakowskitarg.HomeActivityScope;
import com.example.bartosz.krakowskitarg.MyApplicationComponent;
import com.example.bartosz.krakowskitarg.authorization.AuthorizationActivity;
import com.example.bartosz.krakowskitarg.authorization.login.view.AuthorizationFragment;

import dagger.Component;

/**
 * Created by Bartosz on 07.04.2017.
 */
@HomeActivityScope
@Component(modules = AuthorizationModule.class, dependencies = MyApplicationComponent.class)
public interface AuthorizationComponent {
    void inject(@NonNull AuthorizationFragment fragment);
    void inject( AuthorizationActivity fragment);

}


