package com.example.bartosz.krakowskitarg.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.bartosz.krakowskitarg.R;
import com.example.bartosz.krakowskitarg.base.util.Util;

import butterknife.ButterKnife;
import butterknife.Unbinder;

import javax.inject.Inject;

import timber.log.Timber;

public abstract class BaseFragment<PRESENTER extends Presenter<VIEW>, VIEW extends BaseView>
        extends Fragment implements BaseView {

//  @Inject protected DrawerManager drawerManager;

    private Unbinder mUnbinder;

//  @Override public void logoutUser(String errorCode) {
//    BaseActivity activity = (BaseActivity) getActivity();
//    if (activity != null) activity.logoutUser(errorCode);
//  }

    public abstract int getLayoutId();

    @Override
    public String getStringFromId(int id) {
        return getString(id);
    }

    @Override
    public void showDialog(String message) {
        Util.showDialog(getContext(), message);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initInjection();
    }

    @Override
    public void logoutUser(String errorMessage) {

    }

    //called in on create
    protected abstract void initInjection();

    @Override
    public void onResume() {
        super.onResume();
        getPresenter().onViewVisible();
    }

    @Override
    public void onPause() {
        super.onPause();
        getPresenter().onViewHidden();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = null;
        if (getMenuSettings() == SetMenu.NONE) {
//       kontenery
            view = inflater.inflate(getLayoutId(), container, false);
            mUnbinder = ButterKnife.bind(this, view);
            if (view.findViewById(R.id.back_button) != null)
                view.findViewById(R.id.back_button).setOnClickListener(v -> getActivity().onBackPressed());
            return view;
        }
        if (getMenuSettings() == SetMenu.BOTH) {
            view = inflater.inflate(getLayoutId(), container, false);

            mUnbinder = ButterKnife.bind(this, view);
            if (view.findViewById(R.id.back_button) != null)
                view.findViewById(R.id.back_button).setOnClickListener(v -> getActivity().onBackPressed());

            return view;
        }

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getPresenter().onViewCreated((VIEW) this);
    }

    protected abstract PRESENTER getPresenter();

    @Override
    public void onDestroyView() {
        if (getPresenter() != null) getPresenter().onViewDestroyed();
        super.onDestroyView();
        if (mUnbinder != null) mUnbinder.unbind();
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void log(String message) {
        Timber.d(message);
    }

    public SetMenu getMenuSettings() {
        return SetMenu.NONE;
    }

    public enum SetMenu {
        BOTH, NONE
    }
}
