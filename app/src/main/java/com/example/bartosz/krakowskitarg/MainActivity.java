package com.example.bartosz.krakowskitarg;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.bartosz.krakowskitarg.addoffer.view.AddOfferFragment;
import com.example.bartosz.krakowskitarg.authorization.AuthorizationActivity;
import com.example.bartosz.krakowskitarg.menu.menulist.view.MenuFragment;
import com.example.bartosz.krakowskitarg.offers.detalis.view.OfferDetailsFragment;
import com.example.bartosz.krakowskitarg.offers.main.view.OfferFragment;

public class MainActivity extends AppCompatActivity {


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            switch (item.getItemId()) {
                case R.id.navigation_main_offer:
                    Fragment offerFragment = new OfferFragment();
                    fragmentTransaction.replace(R.id.fragment_container, offerFragment);
                    fragmentTransaction.commit();
                    return true;

                case R.id.navigation_user_offer:
                    Fragment addOfferFragment = new AddOfferFragment();
                    fragmentTransaction.replace(R.id.fragment_container, addOfferFragment);
                    fragmentTransaction.commit();
                    return true;
                case R.id.navigation_logout:
                    Fragment menuFragment = new MenuFragment();
                    fragmentTransaction.replace(R.id.fragment_container, menuFragment);
                    fragmentTransaction.commit();
                    return true;
            }
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment fragment = new OfferFragment();
        fragmentTransaction.replace(R.id.fragment_container, fragment);
        fragmentTransaction.commit();
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

}
