package com.example.bartosz.krakowskitarg.base;

import lombok.Getter;

/**
 * Created by Bartosz on 09.04.2017.
 */
@Getter
public class AuthorizationToken {
    String id_token;
}
