package com.example.bartosz.krakowskitarg.data.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Bartosz on 10.04.2017.
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
public class RegisterRequest {
    String username;
    String password;
    String email;

}
