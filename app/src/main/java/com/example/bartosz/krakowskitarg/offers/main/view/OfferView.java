package com.example.bartosz.krakowskitarg.offers.main.view;

import com.example.bartosz.krakowskitarg.base.BaseView;
import com.example.bartosz.krakowskitarg.data.model.Offer;

import java.util.List;

/**
 * Created by Bartosz on 24.04.2017.
 */

public interface OfferView extends BaseView {
    void renderOffer(List<Offer> offerList);
}
