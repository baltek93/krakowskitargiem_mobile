package com.example.bartosz.krakowskitarg.base;

/**
 * Created by Bartosz on 26.03.2017.
 */

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.example.bartosz.krakowskitarg.BuildConfig;
import com.example.bartosz.krakowskitarg.ContextModule;
import com.example.bartosz.krakowskitarg.MyApplicationScope;
import com.example.bartosz.krakowskitarg.base.ApplicationContext;

import java.io.File;

import dagger.Module;
import dagger.Provides;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import timber.log.Timber;

@Module(includes = ContextModule.class) public class NetworkModule {

  @Provides @MyApplicationScope public HttpLoggingInterceptor loggingInterceptor() {
    HttpLoggingInterceptor interceptor =
        new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
          @Override public void log(String message) {
            Timber.i(message);
          }
        });
    interceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);
    return interceptor;
  }

  @Provides @MyApplicationScope public Cache cache(File cacheFile) {
    return new Cache(cacheFile, 10 * 1000 * 1000); //10MB Cahe
  }

  @Provides @MyApplicationScope public File cacheFile(@ApplicationContext Context context) {
    return new File(context.getCacheDir(), "okhttp_cache");
  }

  @Provides @MyApplicationScope
  public OkHttpClient okHttpClient(HttpLoggingInterceptor loggingInterceptor, Cache cache) {
    //// FIXME: 2016-06-30 remove logging if release version
    OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
    SSLContext sslContext = null;
    //// FIXME: 2016-11-18 TS-OMG-FIX-THIS-ASAP
    X509TrustManager x509TrustManager = new X509TrustManager() {
      @Override public void checkClientTrusted(X509Certificate[] chain, String authType)
          throws CertificateException {
      }

      @Override public void checkServerTrusted(X509Certificate[] chain, String authType)
          throws CertificateException {
      }

      @Override public X509Certificate[] getAcceptedIssuers() {
        return new X509Certificate[0];
      }
    };
    final TrustManager[] trustAllCerts = new TrustManager[] {
        x509TrustManager
    };

    try {
      sslContext = SSLContext.getInstance("SSL");
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    }
    try {
      sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
    } catch (KeyManagementException e) {
      e.printStackTrace();
    }
    builder.sslSocketFactory(sslContext.getSocketFactory(), x509TrustManager);
    builder.hostnameVerifier((hostname, session) -> true);
    if (BuildConfig.DEBUG) {
      HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
      interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
      builder.addInterceptor(interceptor);
      builder.readTimeout(6, TimeUnit.MINUTES);
    }
    return builder.cache(cache).build();
  }
}
