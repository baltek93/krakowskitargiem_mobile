package com.example.bartosz.krakowskitarg;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.example.bartosz.krakowskitarg.base.AppModule;
import com.example.bartosz.krakowskitarg.base.NetworkModule;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import retrofit2.Retrofit;
import timber.log.Timber;

public class MyApplication extends Application {

    private MyApplicationComponent component;
    private Retrofit retrofit;

    public static MyApplication get(Activity activity) {
        return (MyApplication) activity.getApplication();
    }

//    @Override
//    protected void attachBaseContext(Context base) {
//        super.attachBaseContext(base);
//        MultiDex.install(this);
//    }

    private void initRealmConfiguration() {
        Realm.init(getApplicationContext());
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);
    }

    @Override
    public void onCreate() {
        super.onCreate();

//        Timber.plant(new Timber.DebugTree());
        initRealmConfiguration();
        component = DaggerMyApplicationComponent.builder()
                .contextModule(new ContextModule(this))
                .networkModule(new NetworkModule())
                .appModule(new AppModule(this))
                .build();
        Retrofit retrofit = component.getRetrofit();
        Realm realm = Realm.getDefaultInstance();
    }

    public MyApplicationComponent component() {
        return component;
    }
}