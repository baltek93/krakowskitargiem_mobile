package com.example.bartosz.krakowskitarg.data.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.LocalDate;

/**
 * Created by Bartosz on 24.04.2017.
 */
@Getter
@Setter
@Builder
@AllArgsConstructor

public class Offer {
  private String name;
  private String description;
  private Double price;
  private String date;
}
