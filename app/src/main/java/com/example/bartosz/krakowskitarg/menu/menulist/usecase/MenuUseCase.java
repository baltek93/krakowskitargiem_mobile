package com.example.bartosz.krakowskitarg.menu.menulist.usecase;

import android.content.SharedPreferences;

import com.example.bartosz.krakowskitarg.base.UseCase;
import com.example.bartosz.krakowskitarg.data.Repository;

import retrofit2.Retrofit;

/**
 * Created by Bartosz on 22.05.2017.
 */

public class MenuUseCase extends UseCase {
    public MenuUseCase(Repository mRepository, Retrofit retrofit, SharedPreferences sharedPreferences) {
        super(mRepository, retrofit, sharedPreferences);
    }
}
