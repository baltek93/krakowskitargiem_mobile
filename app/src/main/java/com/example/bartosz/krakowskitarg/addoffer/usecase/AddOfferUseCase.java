package com.example.bartosz.krakowskitarg.addoffer.usecase;

import android.content.SharedPreferences;

import com.example.bartosz.krakowskitarg.base.UseCase;
import com.example.bartosz.krakowskitarg.data.Repository;

import retrofit2.Retrofit;

/**
 * Created by Bartosz on 21.05.2017.
 */

public class AddOfferUseCase extends UseCase {
    public AddOfferUseCase(Repository mRepository, Retrofit retrofit, SharedPreferences sharedPreferences) {
        super(mRepository, retrofit, sharedPreferences);
    }
}
