package com.example.bartosz.krakowskitarg.authorization.login.di;

import android.content.SharedPreferences;

import com.example.bartosz.krakowskitarg.HomeActivityScope;
import com.example.bartosz.krakowskitarg.authorization.login.presenter.AuthorizationPresenter;
import com.example.bartosz.krakowskitarg.authorization.login.usecase.AuthorizationUseCase;
import com.example.bartosz.krakowskitarg.data.Repository;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by Bartosz on 07.04.2017.
 */
@Module
public class AuthorizationModule {

    @Provides
    @HomeActivityScope
    public AuthorizationUseCase providesAuthorizationUseCase(Repository repository, Retrofit retrofit,SharedPreferences sharedPreferences) {
        return new AuthorizationUseCase(repository,retrofit,sharedPreferences);
    }
    @Provides
    @HomeActivityScope
    public AuthorizationPresenter providesAuthorizationPresenter(AuthorizationUseCase usecase) {
        return new AuthorizationPresenter(usecase);
    }
}

