package com.example.bartosz.krakowskitarg.menu.menulist.view;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.bartosz.krakowskitarg.MainActivity;
import com.example.bartosz.krakowskitarg.R;
import com.example.bartosz.krakowskitarg.authorization.AuthorizationActivity;
import com.example.bartosz.krakowskitarg.data.model.Offer;
import com.example.bartosz.krakowskitarg.offers.detalis.view.OfferDetailsFragment;

import java.util.List;

/**
 * Created by Bartosz on 22.05.2017.
 */

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.MyViewHolder> {

    private List<MenuFragment.MenuItem> menuItems;
    private MainActivity activity;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public ImageView image;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.label_menu);
            image = (ImageView) view.findViewById(R.id.icon_menu);
        }
    }

    public MenuAdapter(List<MenuFragment.MenuItem> offerList, MainActivity activity) {
        this.menuItems = offerList;
        this.activity = activity;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView =
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_menu, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.itemView.setOnClickListener(v -> {
            switch (position)
            {
                case 0:
                    break;
                case 1:
                    activity.finish();
                    activity.startActivity(new Intent(activity, AuthorizationActivity.class));
                    break;
            }
            FragmentManager fragmentManager = activity.getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.addToBackStack(null);
            Fragment fragment = new OfferDetailsFragment();
            fragmentTransaction.replace(R.id.fragment_container, fragment);
            fragmentTransaction.commit();

        });
        MenuFragment.MenuItem offer = menuItems.get(position);
        holder.name.setText(offer.getLabelMenuResources());
        holder.image.setBackground(activity.getResources().getDrawable(offer.getIdmenuResources()));
    }

    @Override
    public int getItemCount() {
        if (menuItems != null) {
            return menuItems.size();
        } else
            return 0;
    }
}

