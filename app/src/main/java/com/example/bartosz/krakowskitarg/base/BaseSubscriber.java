package com.example.bartosz.krakowskitarg.base;

import com.fernandocejas.frodo.BuildConfig;
import com.fernandocejas.frodo.annotation.RxLogSubscriber;

import org.reactivestreams.Subscriber;

import io.reactivex.disposables.Disposable;
import timber.log.Timber;

@RxLogSubscriber
public abstract class BaseSubscriber<T, PRESENTER extends Presenter>
        implements Subscriber<T> {

  private final PRESENTER mPresenter;

  public BaseSubscriber(PRESENTER presenter) {
    mPresenter = presenter;
  }

  public PRESENTER getPresenter() {
    return mPresenter;
  }

  @Override
  public void onComplete() {

  }

  @Override public void onError(Throwable e) {

  }

  @Override public void onNext(T t) {
  }
}
