package com.example.bartosz.krakowskitarg.menu.menulist.presenter;

import com.example.bartosz.krakowskitarg.base.Presenter;
import com.example.bartosz.krakowskitarg.base.UseCase;
import com.example.bartosz.krakowskitarg.menu.menulist.usecase.MenuUseCase;
import com.example.bartosz.krakowskitarg.menu.menulist.view.MenuView;

/**
 * Created by Bartosz on 22.05.2017.
 */

public class MenuPresenter extends Presenter<MenuView> {
    MenuUseCase mUseCase;

    public MenuPresenter(MenuUseCase mUseCase) {
        super(mUseCase);
        this.mUseCase = mUseCase;
    }
}
