package com.example.bartosz.krakowskitarg.data;

import com.example.bartosz.krakowskitarg.base.AuthorizationToken;
import com.example.bartosz.krakowskitarg.data.model.LoginRequest;
import com.example.bartosz.krakowskitarg.data.model.Offer;
import com.example.bartosz.krakowskitarg.data.model.RegisterRequest;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by Bartosz on 26.03.2017.
 */
public interface RemoteRepositoryInterface {

    @POST("api/authenticate")
    Call<AuthorizationToken> authorization(@Body LoginRequest loginRequest);

    @POST("api/register")
    Call<Void> register(@Body RegisterRequest registerRequest);

    @GET("api/offers")
    Call<List<Offer>> getOffer(@Header("Authorization") String authorization);

    @POST("api/offers")
    Call<List<Offer>> createOffer(@Header("Authorization") String authorization, @Body Offer offer);

}
