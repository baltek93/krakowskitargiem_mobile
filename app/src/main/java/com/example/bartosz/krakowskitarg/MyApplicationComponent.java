package com.example.bartosz.krakowskitarg;

/**
 * Created by Bartosz on 26.03.2017.
 */


import android.content.SharedPreferences;

import com.example.bartosz.krakowskitarg.base.AppModule;
import com.example.bartosz.krakowskitarg.data.Repository;

import dagger.Component;
import retrofit2.Retrofit;

@MyApplicationScope
@Component(modules = {AppModule.class})
public interface MyApplicationComponent {

    Retrofit getRetrofit();

    Repository getRepository();

    SharedPreferences getSharePreferences();
}
