package com.example.bartosz.krakowskitarg.offers.detalis.view;

import com.example.bartosz.krakowskitarg.MyApplication;
import com.example.bartosz.krakowskitarg.R;
import com.example.bartosz.krakowskitarg.base.BaseFragment;
import com.example.bartosz.krakowskitarg.offers.detalis.presenter.OfferDetailsPresenter;
import com.example.bartosz.krakowskitarg.offers.di.DaggerOfferComponent;
import com.example.bartosz.krakowskitarg.offers.di.OfferModule;

import javax.inject.Inject;


public class OfferDetailsFragment extends BaseFragment<OfferDetailsPresenter, OfferDetailsView> implements OfferDetailsView {

    @Inject
    OfferDetailsPresenter presenter;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_offer_details;
    }

    @Override
    protected void initInjection() {
        DaggerOfferComponent.builder()
                .offerModule(new OfferModule())
                .myApplicationComponent(MyApplication.get(getActivity()).component())
                .build().inject(this);
    }

    @Override
    protected OfferDetailsPresenter getPresenter() {
        return presenter;
    }
}
