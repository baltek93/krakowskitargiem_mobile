package com.example.bartosz.krakowskitarg.addoffer.view;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.bartosz.krakowskitarg.MainActivity;
import com.example.bartosz.krakowskitarg.R;
import com.example.bartosz.krakowskitarg.data.model.Offer;
import com.example.bartosz.krakowskitarg.offers.detalis.view.OfferDetailsFragment;

import java.util.List;

/**
 * Created by Bartosz on 24.04.2017.
 */

public class AddOfferAdapter extends RecyclerView.Adapter<AddOfferAdapter.MyViewHolder> {

    private List<Offer> offerList;
    private MainActivity activity;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, price, description;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name_offer);
            price = (TextView) view.findViewById(R.id.price_offer);
            description = (TextView) view.findViewById(R.id.description_offer);
        }
    }

    public AddOfferAdapter(List<Offer> offerList, MainActivity activity) {
        this.offerList = offerList;
        this.activity = activity;
    }

    public void setOfferList(List<Offer> offerList) {
        this.offerList = offerList;
        notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView =
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_offer, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.itemView.setOnClickListener(v -> {
            FragmentManager fragmentManager = activity.getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.addToBackStack(null);
            Fragment fragment = new OfferDetailsFragment();
            fragmentTransaction.replace(R.id.fragment_container, fragment);
            fragmentTransaction.commit();

        });
        Offer offer = offerList.get(position);
        holder.name.setText(offer.getName());
        holder.price.setText(offer.getPrice().toString());
        holder.description.setText(offer.getDescription());
    }

    @Override
    public int getItemCount() {
        if (offerList != null) {
            return offerList.size();
        } else
            return 0;
    }
}