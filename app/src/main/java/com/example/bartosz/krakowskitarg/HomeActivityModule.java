package com.example.bartosz.krakowskitarg;

import com.example.bartosz.krakowskitarg.authorization.AuthorizationActivity;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Bartosz on 26.03.2017.
 */


@Module
public class HomeActivityModule {

    private AuthorizationActivity homeActivity;
    public String abc;

    public HomeActivityModule(AuthorizationActivity homeActivity) {
        this.homeActivity = homeActivity;
    }

    @Provides
    @HomeActivityScope
    public AuthorizationActivity homeActivity() {
        return homeActivity;
    }


}
